from amaranth import *

class CoinCounter(Elaboratable):
  """
  Computes the number of cents for any coins inserted.
  """

  def __init__(self):
    self.pennies = Signal(8)
    self.nickels = Signal(4)
    self.dimes = Signal(4)
    self.quarters = Signal(4)
    self.dollars = Signal(4)
    self.cents = Signal(12)
    self.foo = Signal(16)

  def elaborate(self, platform):
    m = Module()
    m.d.comb += self.cents.eq(self.pennies + 5*self.nickels + 10*self.dimes + 25*self.quarters + 100*self.dollars)
    m.d.sync += self.foo.eq(0)
    return m

# --- TEST ---
from amaranth.sim import Simulator

dut = CoinCounter()
def bench():
  yield dut.dollars.eq(2)
  yield dut.quarters.eq(5)
  yield dut.dimes.eq(10)
  yield dut.nickels.eq(3)
  yield dut.pennies.eq(37)
  yield
  assert (yield dut.cents == 477)

  yield dut.dollars.eq(5)
  yield dut.quarters.eq(1)
  yield dut.dimes.eq(2)
  yield dut.nickels.eq(0)
  yield dut.pennies.eq(3)
  yield
  assert (yield dut.cents == 548)

  yield dut.dollars.eq(0)
  yield dut.quarters.eq(1)
  yield dut.dimes.eq(1)
  yield dut.nickels.eq(2)
  yield dut.pennies.eq(19)
  yield
  assert (yield dut.cents == 64)

sim = Simulator(dut)
sim.add_clock(1e-6) # 1 MHz
sim.add_sync_process(bench)
with sim.write_vcd("main.vcd"):
  sim.run()
