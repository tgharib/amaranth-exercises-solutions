import itertools

from amaranth import *
from amaranth.build import *
# from amaranth_boards.icebreaker import *
from amaranth_boards.lifcl_evn import *

class LEDBlinker(Elaboratable):
  def elaborate(self, platform):
    m = Module()

    def get_all_resources(name):
      resources = []
      for number in itertools.count():
        try:
          resources.append(platform.request(name, number))
        except ResourceError:
          break
      return resources

    leds = [res.o for res in get_all_resources("led")]
    buttons = [res.i for res in get_all_resources("button")]

    half_freq = int(platform.default_clk_frequency // 2)
    timer = Signal(range(half_freq + 1))

    with m.If(timer == half_freq):
      m.d.sync += leds[0].eq(~leds[0])
      m.d.sync += timer.eq(0)
    with m.Else():
      m.d.sync += timer.eq(timer + 1)

    # add amaranth version of and_gate logic of this lattice radiant getting started tutorial https://www.youtube.com/watch?v=aIvPzghJJZQ
    # on crosslinknx eval board, buttons[0]=SW2 and buttons[1]=SW3
    m.d.comb += leds[1].eq(buttons[0] & buttons[1])

    return m

# ICEBreakerPlatform().build(LEDBlinker(), do_program=True)
LIFCLEVNPlatform().build(LEDBlinker(), do_program=True)
