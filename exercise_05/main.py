from amaranth import *

class Main(Elaboratable):
  def __init__(self):
    self.count = Signal(4, reset=1)

  def elaborate(self, platform):
    m = Module()

    with m.If(self.count == 9):
      m.d.sync += self.count.eq(1)
    with m.Else():
      m.d.sync += self.count.eq(self.count + 1)
    return m

# --- TEST ---
from amaranth.sim import Simulator

dut = Main()
def bench():
  assert (yield dut.count == 1)
  yield
  assert (yield dut.count == 2)
  yield
  assert (yield dut.count == 3)
  yield
  assert (yield dut.count == 4)
  yield
  assert (yield dut.count == 5)
  yield
  assert (yield dut.count == 6)
  yield
  assert (yield dut.count == 7)
  yield
  assert (yield dut.count == 8)
  yield
  assert (yield dut.count == 9)
  yield
  assert (yield dut.count == 1)

sim = Simulator(dut)
sim.add_clock(1e-6) # 1 MHz
sim.add_sync_process(bench)
with sim.write_vcd("main.vcd"):
  sim.run()
