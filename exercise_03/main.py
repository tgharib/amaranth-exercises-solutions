from amaranth import *

class GameOfLife(Elaboratable):
  """
  Plays game of life.
  """

  def __init__(self):
    self.input = Signal(9)
    self.output = Signal(1)
    self.foo = Signal(1)

  def elaborate(self, platform):
    m = Module()

    # if middle cell is alive with 2 or 3 alive neighbors (2 or 3 neighbors + 1 alive middle cell)
    with m.If((self.input[4] == 1) & ((sum(self.input) == 3) | (sum(self.input) == 4))):
      m.d.comb += self.output.eq(1)
    # if middle cell is dead with 3 alive neighbors (3 neighbors + 0 alive middle cell)
    with m.Elif((self.input[4] == 0) & (sum(self.input) == 3)):
      m.d.comb += self.output.eq(1)
    with m.Else():
      m.d.comb += self.output.eq(0)

    m.d.sync += self.foo.eq(self.output)
    return m

# --- TEST ---
from amaranth.sim import Simulator

dut = GameOfLife()
def bench():
  # Input:
  # 1 1 1
  # 1 1 1
  # 1 1 1
  yield dut.input.eq(0x1FF)
  yield
  assert (yield dut.output == 0)

  # Input:
  # 0 0 1
  # 0 1 0
  # 1 0 0
  yield dut.input.eq(0x54)
  yield
  assert (yield dut.output == 1)

  # Input:
  # 0 1 1
  # 0 1 0
  # 1 0 0
  yield dut.input.eq(0xD4)
  yield
  assert (yield dut.output == 1)

  # Input:
  # 0 1 1
  # 0 0 0
  # 1 0 0
  yield dut.input.eq(0xC4)
  yield
  assert (yield dut.output == 1)

  # Input:
  # 0 1 0
  # 0 0 0
  # 1 0 0
  yield dut.input.eq(0x84)
  yield
  assert (yield dut.output == 0)

sim = Simulator(dut)
sim.add_clock(1e-6) # 1 MHz
sim.add_sync_process(bench)
with sim.write_vcd("main.vcd"):
  sim.run()
