from amaranth import *

class NextDayComputer(Elaboratable):
  """
  Computes the next day for an input date.
  """

  def __init__(self):
    self.day = Signal(5)
    self.month = Signal(4)
    self.year = Signal(14)
    self.next_day = Signal.like(self.day)
    self.next_month = Signal.like(self.month)
    self.next_year = Signal.like(self.year)
    self.days_in_month = Signal(5)
    self.invalid = Signal(1)
    self.foo = Signal(16)

  def elaborate(self, platform):
    m = Module()

    # Compute the number of days in a month
    with m.Switch(self.month):
      with m.Case(1):
        m.d.comb += self.days_in_month.eq(31)
      with m.Case(2):
        with m.If(((self.year % 4 == 0) & (self.year % 100 != 0)) | (self.year % 400 == 0)):
          m.d.comb += self.days_in_month.eq(29)
        with m.Else():
          m.d.comb += self.days_in_month.eq(28)
      with m.Case(3):
        m.d.comb += self.days_in_month.eq(31)
      with m.Case(4):
        m.d.comb += self.days_in_month.eq(30)
      with m.Case(5):
        m.d.comb += self.days_in_month.eq(31)
      with m.Case(6):
        m.d.comb += self.days_in_month.eq(30)
      with m.Case(7):
        m.d.comb += self.days_in_month.eq(31)
      with m.Case(8):
        m.d.comb += self.days_in_month.eq(31)
      with m.Case(9):
        m.d.comb += self.days_in_month.eq(30)
      with m.Case(10):
        m.d.comb += self.days_in_month.eq(31)
      with m.Case(11):
        m.d.comb += self.days_in_month.eq(30)
      with m.Default():
        m.d.comb += self.days_in_month.eq(31)

    # Compute next day
    with m.If(self.day < self.days_in_month):
      m.d.comb += self.next_day.eq(self.day + 1)
      m.d.comb += self.next_month.eq(self.month)
      m.d.comb += self.next_year.eq(self.year)
    with m.Else():
      m.d.comb += self.next_day.eq(1)
      with m.If(self.month < 12):
        m.d.comb += self.next_month.eq(self.month + 1)
        m.d.comb += self.next_year.eq(self.year)
      with m.Else():
        m.d.comb += self.next_month.eq(1)
        m.d.comb += self.next_year.eq(self.year + 1)

    # Check for invalid dates
    with m.If((self.day < 1) | (self.day > self.days_in_month) | (self.month < 1) | (self.month > 12) | (self.year < 1) | (self.year > 9999)):
      m.d.comb += self.invalid.eq(1)
      m.d.comb += self.next_day.eq(0)
      m.d.comb += self.next_month.eq(0)
      m.d.comb += self.next_year.eq(0)

    m.d.sync += self.foo.eq(self.days_in_month | self.next_day)
    return m

# --- TEST ---
from amaranth.sim import Simulator

dut = NextDayComputer()
def bench():
  # Test simple day increment
  yield dut.year.eq(2000)
  yield dut.month.eq(1)
  yield dut.day.eq(1)
  yield
  assert (yield dut.next_year == 2000)
  assert (yield dut.next_month == 1)
  assert (yield dut.next_day == 2)

  # Test that January has 31 days
  yield dut.year.eq(2000)
  yield dut.month.eq(1)
  yield dut.day.eq(30)
  yield
  assert (yield dut.next_year == 2000)
  assert (yield dut.next_month == 1)
  assert (yield dut.next_day == 31)

  # Test that January has 31 days
  yield dut.year.eq(2000)
  yield dut.month.eq(1)
  yield dut.day.eq(31)
  yield
  assert (yield dut.next_year == 2000)
  assert (yield dut.next_month == 2)
  assert (yield dut.next_day == 1)

  # Test that 2000 is a leap year
  yield dut.year.eq(2000)
  yield dut.month.eq(2)
  yield dut.day.eq(29)
  yield
  assert (yield dut.next_year == 2000)
  assert (yield dut.next_month == 3)
  assert (yield dut.next_day == 1)

  # Test that 1900 isn't a leap year
  yield dut.year.eq(1900)
  yield dut.month.eq(2)
  yield dut.day.eq(28)
  yield
  assert (yield dut.next_year == 1900)
  assert (yield dut.next_month == 3)
  assert (yield dut.next_day == 1)

  # Test an invalid date
  yield dut.year.eq(2000)
  yield dut.month.eq(1)
  yield dut.day.eq(32)
  yield
  assert (yield dut.invalid == 1)
  assert (yield dut.next_year == 0)
  assert (yield dut.next_month == 0)
  assert (yield dut.next_day == 0)

sim = Simulator(dut)
sim.add_clock(1e-6) # 1 MHz
sim.add_sync_process(bench)
with sim.write_vcd("main.vcd"):
  sim.run()
